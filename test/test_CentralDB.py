import unittest
from CentralDB import CentralDB
from unittest.mock import Mock, patch

class TestCDB(unittest.TestCase):
    @patch('CentralDB.redis')
    def setUp(self, mock_redis):
        self.channel = 'channel'
        self.port = 50000

        self.mock_client = Mock()
        self.mock_cache = Mock()

        self.mock_redis_connection = Mock(return_value=self.mock_client)
        self.mock_strict_redis = Mock(return_value=self.mock_cache)
        
        self.mock_scan_iter = Mock(return_value=[self.channel])

        self.mock_get = Mock(return_value=bytes(str(self.port), 'utf-8'))
        self.mock_set = Mock()
        self.mock_keys = Mock(return_value=['pattern_a', 'pattern_b'])
        self.mock_delete = Mock()

        self.mock_client.get = self.mock_get
        self.mock_client.set = self.mock_set
        self.mock_client.keys = self.mock_keys
        self.mock_client.delete = self.mock_delete

        self.mock_cache.scan_iter = self.mock_scan_iter

        mock_redis.Redis = self.mock_redis_connection
        mock_redis.StrictRedis = self.mock_strict_redis

        self.cdb = CentralDB()
    def tearDown(self):
        pass

    @patch('CentralDB.redis')
    def test_init(self, mock_redis):
        '''
        [init] should connect to redis
        '''
        self.mock_redis_connection.assert_called_once()
        self.mock_strict_redis.assert_called_once()
    
    
    def test_get_nc_port(self):
        '''
        [get_nc_port] should add prefix nc_ to channel
        '''
        result = self.cdb.get_nc_port(self.channel)

        self.mock_get.assert_called_once_with('nc_{}'.format(self.channel))
        self.assertEqual(result, self.port)

    def test_set_nc_port(self):
        '''
        [set_nc_port] should add prefix nc_ to channel
        '''
        self.cdb.set_nc_port(self.channel, self.port)
        self.mock_set.assert_called_once_with('nc_{}'.format(self.channel), self.port)
        
    def test_get_all_nc_port(self):
        '''
        [get_all_nc_port] should get all key start with nc_
        '''
        self.cdb.get_all_nc_port()
        self.mock_scan_iter.assert_called_once_with('nc_*')
    
    def test_get(self):
        '''
        [get]
        '''
        result = self.cdb.get(self.channel)
        self.mock_get.assert_called_once_with(self.channel)
        self.assertEqual(result, str(self.port))

    def test_set(self):
        '''
        [set]
        '''
        result = self.cdb.set(self.channel, self.port)
        self.mock_set.assert_called_once_with(self.channel, str(self.port))

    def test_reset_subscan(self):
        mock_reset_pattern = Mock()
        self.cdb.reset_pattern = mock_reset_pattern

        self.cdb.reset_subscan()
        mock_reset_pattern.assert_called_once_with('SUBS_*')

    def test_reset_obs(self):
        mock_reset_pattern = Mock()
        self.cdb.reset_pattern = mock_reset_pattern

        self.cdb.reset_obs()
        mock_reset_pattern.assert_called_once_with('[^nc_]*')

    def test_reset_pattern(self):
        pattern = 'mock_pattern'
        self.cdb.reset_pattern(pattern)
        self.mock_keys.assert_called_once_with(pattern)
        self.mock_delete.assert_called_once()