import logging
import redis
import cdbConfig as config
import socket

class CentralDB:
    def __init__(self):
        # Logger for this CentralDB
        self.logger = logging.getLogger("CentralDB")

        # Connect to dDB
        self.client = redis.Redis(
            host=config.HOST, 
            port=config.PORT,
        )
        self.cache = redis.StrictRedis()
    
    def __del__(self):
        pass
    
    def get_nc_port(self, channel):
        db_key = 'nc_{}'.format(channel)
        nc_port = int(self.client.get(db_key))
        return nc_port
    
    def set_nc_port(self, channel, port):
        db_key = 'nc_{}'.format(channel)
        self.client.set(db_key, port)

    def get_all_nc_port(self):
        return self.cache.scan_iter('nc_*')

    def get(self, key, dtype=None):
        try:
            value = self.client.get(key).decode('utf-8')
            if dtype != None:
                return value and dtype(value)
            return value
        except AttributeError:
            return None
    
    def set(self, key, value):
        self.client.set(key, str(value))

    def reset_subscan(self):
        self.reset_pattern('SUBS_*')

    def reset_obs(self):
        # Reset all Redis Key except keys for notification channel
        # This function will be used to clean up before starting new scan
        self.reset_pattern('[^nc_]*')

    def reset_pattern(self, pattern):
        keys = self.client.keys(pattern)
        if len(keys) > 0:
            self.client.delete(*keys)

    def get_connection_info(self):
        connection_info = {
            'acu_ip': socket.gethostbyname("acu"),
            'mock_acu_ip': socket.gethostbyname("mockAcu"),
            'cmd_port': 9000,
            'sts_port': 9001,
            'edd_master_controller_host': socket.gethostbyname("edd_master_controller"),
            'edd_master_controller_port': 7147,
            'edd_redis_host': socket.gethostbyname("edd_redis"),
            'edd_redis_port': 6333,
            'edd_redis_port': socket.gethostbyname("holofft"),
            'holofft_port': 1234,
            'powermeter_host': socket.gethostbyname("powermeter"),
        }
        return connection_info
    
    def set_connection_info(self):
        connection_info = {
            'acu_ip': socket.gethostbyname("acu"),
            'mock_acu_ip': socket.gethostbyname("mockAcu"),
            'cmd_port': 9000,
            'sts_port': 9001,
            'edd_master_controller_host': socket.gethostbyname("edd_master_controller"),
            'edd_master_controller_port': 7147,
            'edd_redis_host': socket.gethostbyname("edd_redis"),
            'edd_redis_port': 6333,
            'holofft_host': socket.gethostbyname("holofft"),
            'holofft_port': 1234,
            'powermeter_host': socket.gethostbyname("powermeter"),
        }

        # Set each key-value pair in Redis
        for key, value in connection_info.items():
            self.set(key, value)

if __name__ == "__main__":
    try:
        db = CentralDB()
        
        db.set_nc_port('test_nc_port', 1234)
        print(db.get_nc_port('test_nc_port'))

        db.set('test_key', 'test_value')
        print(db.get('test_key'))

        db.set_connection_info()
        print(db.get_connection_info())

    except KeyboardInterrupt:
        print('KeyboardInterrupt')
    