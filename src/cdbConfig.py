import os

HOST = os.getenv('DB_HOST')
PORT = int(os.getenv('DB_PORT'))
PASSWORD = os.getenv('DB_PASSWORD')