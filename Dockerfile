FROM redis:latest

COPY flushall.sh /usr/local/bin/

RUN chmod +x /usr/local/bin/flushall.sh

CMD ["/usr/local/bin/flushall.sh"]