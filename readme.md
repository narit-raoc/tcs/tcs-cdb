Redis and Redis GUI
---
```
cd tnrt/utils/CentralDB
docker-compose up -d
```

- redis is running on localhost:6379
- GUI is running on http://localhost:7843

**GUI connection**
- **Hostname**: cdb
- **Port**: 6379